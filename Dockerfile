
FROM maven:3-jdk-8-slim

COPY . /sourcesTpTaches

RUN cd /sourcesTpTaches && mvn clean package


ENTRYPOINT ["java","-Djava.security.egd=file:/dev/./urandom","-jar","/sourcesTpTaches/target/tp-taches-0.0.1-SNAPSHOT.jar"]