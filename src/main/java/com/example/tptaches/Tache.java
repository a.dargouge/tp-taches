package com.example.tptaches;

public class Tache {
  private String description;
  private int priorite;
  private boolean terminee;

  public Tache(String description, int priorite, boolean terminee) {
    this.description = description;
    this.priorite = priorite;
    this.terminee = terminee;
  }

  public Tache() {

  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public int getPriorite() {
    return priorite;
  }

  public void setPriorite(int priorite) {
    this.priorite = priorite;
  }

  public boolean isTerminee() {
    return terminee;
  }

  public void setTerminee(boolean terminee) {
    this.terminee = terminee;
  }
}
